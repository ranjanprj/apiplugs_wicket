import math
class NumberFunc:
    def __init__(self):
        pass

    def square(self,num,defn=False):
        if defn:
            return """
                drop function if exists public.square(int8);
                CREATE OR REPLACE FUNCTION public.square(num int8)
                RETURNS int8
                AS $$                
                        
                from apiplugs.extn import NumberFunc
                return NumberFunc().square(num)
                $$ LANGUAGE plpython3u;
                
                create table if not exists extn_metadata(info json); 
                delete from extn_metadata where extn_metadata.info->>'extn_name' = 'square';
                insert into extn_metadata(info) values('
                {
                    "extn_name" :"square",
                    "desc" : "A function that squares a given number",
                    "params": [{"param":"num", "type":"int", "desc":"Number to be squared"}],
                    "returns" : [{"return":"squared_num", "type": "int", "desc" : "Squared Number" }]
                }
                ');
            """

        return num * num

    def multiply(self, num,multiplicant, defn=False):
        if defn:
            return """
                drop function if exists public.multiply(int8,int8);
                CREATE OR REPLACE FUNCTION public.multiply(num int8,multiplicand int8)
                RETURNS int8
                AS $$                

                from apiplugs.extn import NumberFunc
                return NumberFunc().multiply(num,multiplicand)
                $$ LANGUAGE plpython3u;
                 create table if not exists extn_metadata(info json); 
                delete from extn_metadata where extn_metadata.info->>'extn_name' = 'multiply';
                insert into extn_metadata(info) values('
                {
                    "extn_name" :"multiply",
                    "desc" : "A function that multiplies a given number with a multiplicand",
                    "params": [{"param":"num", "type":"int", "desc":"Number to be multiplied"},{"param":"multiplicand", "type":"int", "desc":"multiplicand"}],
                    "returns" : [{"return":"multiplied_number", "type": "int", "desc" : "Multiplied Number" }]
                }
                ');
            """

        return num * multiplicant


    def raise_to(self, num,exp, defn=False):
        if defn:
            return """
                drop function if exists public.raise_to(int8,int8);
                CREATE OR REPLACE FUNCTION public.raise_to(num int8,exp int8)
                RETURNS numeric
                AS $$                

                from apiplugs.extn import NumberFunc
                return NumberFunc().raise_to(num,exp)
                $$ LANGUAGE plpython3u;
                 create table if not exists extn_metadata(info json); 
                delete from extn_metadata where extn_metadata.info->>'extn_name' = 'raise_to';
                insert into extn_metadata(info) values('
                {
                    "extn_name" :"raise_to",
                    "desc" : "A function that returns x^e",
                    "params": [{"param":"num", "type":"int", "desc":"Base number"},{"param":"exp", "type":"int", "desc":"exponent"}],
                    "returns" : [{"return":"exponential_number", "type": "int", "desc" : "Exponential Number" }]
                }
                ');
            """

        return math.pow(num,exp)


    def get_sql_defn(self,defn):

        return  """
                       drop function if exists public.{0}({1});
                       CREATE OR REPLACE FUNCTION public.{0}({2})
                       RETURNS {3}
                       AS $$                

                       from apiplugs.extn import {4}
                       return {4}().{0}({5})
                       $$ LANGUAGE plpython3u;
                        create table if not exists extn_metadata(info json); 
                       delete from extn_metadata where extn_metadata.info->>'extn_name' = '{0}';
                       insert into extn_metadata(info) values('
                       {
                           "extn_name" :"{0}",
                           "desc" : "{6}",
                           "params": {7},
                           "returns" : {8}
                       }
                       ');
                   """.format(defn['name'], defn['param_type_list'], defn['param_list'],defn['return_type'],defn['class_name'],defn['param_invoke'])