from apiplugs.extn import NumberFunc


def register_extn(plpy):
    n = NumberFunc()
    plpy.execute( n.square(None,defn=True) )
    plpy.execute( n.multiply(None,None,defn=True))
    plpy.execute( n.raise_to(None,None,defn=True))



