from setuptools import setup, find_packages
setup(
    name='apiplugs',
    version='0.0.1',
    author='Priya Ranjan',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'requests'
    ],
)


