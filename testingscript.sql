CREATE  EXTENSION if not exists plpython3u;
drop function if exists public.register();
CREATE OR REPLACE FUNCTION public.register()
RETURNS text
AS $$                
from apiplugs import register
return register.register_extn(plpy)
$$ LANGUAGE plpython3u;

select register();

select square(100000000);
select multiply(10,20);


drop function if exists public.square(int8);
CREATE OR REPLACE FUNCTION public.square(num int8)
RETURNS int8
AS $$                
                        
from apiplugs.extn import NumberFunc
return NumberFunc().square(num)
$$ LANGUAGE plpython3u;
select info->>'extn_name' from extn_metadata;
create table if not exists extn_metadata(info json); 
delete from extn_metadata where extn_metadata.info->>'extn_name' = 'square';
insert into extn_metadata(info) values('
                {
                    "extn_name" :"square",
                    "desc" : "A function that squares a given number",
                    "params": [{"param":"num", "type":"int", "desc":"Number to be squared"}],
                    "returns" : [{"return":"num", "type": "int", "desc" : "Squared Number" }]
                }
                ');
                
                